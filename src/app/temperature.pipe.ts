import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'temperature'
})
export class TemperaturePipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    const Fahrenheit = value as number;
    return (5/9)*(Fahrenheit - 32);
  }

}
